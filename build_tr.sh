#!/bin/bash
set -e
export HOME=$(pwd)
WORK_DIR=$HOME
TR_INSDIR=$HOME/_tr
DEPS_DIR=${DEPS_DIR:-"$WORK_DIR/_builds"}
export PKG_CONFIG='pkg-config --static'
export PKG_CONFIG_PATH=$DEPS_DIR/lib/pkgconfig
export PKG_CONFIG_LIBDIR=$DEPS_DIR/lib
export LD_LIBRARY_PATH=$DEPS_DIR/lib

cd $WORK_DIR
curl -L https://github.com/transmission/transmission/releases/download/4.1.0-beta.1/transmission-4.1.0-beta.1+rc3a8106077.tar.xz | tar xJ
TRANSMISSION_SRCDIR=$(pwd)/$(ls | grep transmission-)

b_miniupnpc() {
  cd $TRANSMISSION_SRCDIR/third-party/miniupnp/miniupnpc
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DUPNPC_BUILD_SHARED=0 -DUPNPC_BUILD_TESTS=0 -DUPNPC_BUILD_SAMPLE=0
  make -C_b -j`nproc` install
}

b_b64() {
  cd $TRANSMISSION_SRCDIR/third-party/libb64
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DBUILD_SHARED_LIBS=0 -DLIBB64_ENABLE_TESTS=0 -DLIBB64_BUILD_PROGRAMS=0 -DLIBB64_BUILD_EXAMPLES=0
  make -C_b -j`nproc` install
}

b_deflate() {
  cd $TRANSMISSION_SRCDIR/third-party/libdeflate
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DLIBDEFLATE_BUILD_SHARED_LIB=0 -DLIBDEFLATE_BUILD_GZIP=0 -DLIBDEFLATE_BUILD_TESTS=0 -DLIBDEFLATE_USE_SHARED_LIB=0
  make -C_b -j`nproc` install
}

b_utp() {
  cd $TRANSMISSION_SRCDIR/third-party/libutp
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DLIBUTP_SHARED=0 -DLIBUTP_BUILD_PROGRAMS=0
  make -C_b -j`nproc` install
}

b_natpmp() {
  cd $TRANSMISSION_SRCDIR/third-party/libnatpmp
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DBUILD_SHARED_LIBS=0
  make -C_b -j`nproc` install
}

b_dht() {
  cd $TRANSMISSION_SRCDIR/third-party/dht
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DBUILD_SHARED_LIBS=0
  make -C_b -j`nproc` install
}

[ -f $DEPS_DIR/lib/libdeflate.a ] || b_deflate
[ -f $DEPS_DIR/lib/libminiupnpc.a ] || b_miniupnpc
[ -f $DEPS_DIR/lib/libb64.a ] || b_b64
[ -f $DEPS_DIR/lib/libutp.a ] || b_utp
[ -f $DEPS_DIR/lib/libnatpmp.a ] || b_natpmp
[ -f $DEPS_DIR/lib/libdht.a ] || b_dht
cd $TRANSMISSION_SRCDIR

if [ -n "$BT_NO_BADPEER" ]; then
  echo "Apply patch blocking Xunlei, BaiduNetdisk, QQDownload, etc..."
  cd ./libtransmission
  ../../bt_patch.sh
  cd ..
fi
if [ -n "$PT_FAKE_VERSION" ]; then
  TR_VERNUM=(3 0 0)
  TR_VERTAG=(MAJOR MINOR PATCH)
  echo "Modifing Transmission version to ${TR_VERNUM[0]}.${TR_VERNUM[1]}${TR_VERNUM[2]}"
  for i in {0..2}; do
    sed -Ei "s|set\\(TR_VERSION_${TR_VERTAG[$i]}.+|set(TR_VERSION_${TR_VERTAG[$i]} \"${TR_VERNUM[$i]}\")|" ./CMakeLists.txt
  done
  sed -Ei 's|set\(TR_VERSION_BETA_NUMBER.+|set(TR_VERSION_BETA_NUMBER "")|' ./CMakeLists.txt
  sed -Ei 's|set\(TR_VERSION_DEV.+|set(TR_VERSION_DEV FALSE)|' ./CMakeLists.txt
  sed -Ei 's|set\(TR_SEMVER.+|set(TR_SEMVER "${TR_VERSION_MAJOR}.${TR_VERSION_MINOR}${TR_VERSION_PATCH}")|' ./CMakeLists.txt
fi
if [ -n "$PT_CONTINUE_NONEXISTS" ]; then
  sed -Ei 's|(/\* don\Wt allow the torrent to be started if the files disappeared) \*/|\1|' ./libtransmission/torrent.cc
fi
cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$TR_INSDIR -DCMAKE_PREFIX_PATH=$DEPS_DIR \
  -DENABLE_GTK=0 -DENABLE_QT=0 -DENABLE_MAC=0 -DENABLE_UTILS=0 -DENABLE_TESTS=0 -DENABLE_NLS=0 \
  -DWITH_CRYPTO=openssl -DWITH_SYSTEMD=0 -DINSTALL_DOC=0 -DUSE_SYSTEM_DEFLATE=1 -DUSE_SYSTEM_DHT=1 \
  -DUSE_SYSTEM_MINIUPNPC=1 -DUSE_SYSTEM_NATPMP=1 -DUSE_SYSTEM_UTP=1 -DUSE_SYSTEM_B64=1 \
  -DCMAKE_C_FLAGS="-fPIE -DCARES_STATICLIB -DNGHTTP2_STATICLIB -DNGHTTP3_STATICLIB -DNGTCP2_STATICLIB -L$DEPS_DIR/lib"
sed -i 's|/libpsl.a|/libpsl.a -lidn2 -lunistring -liconv|' _b/daemon/CMakeFiles/transmission-daemon.dir/link.txt

make -j`nproc` -C "$(pwd)/_b" install

cd $HOME
mv $TR_INSDIR/bin/transmission-daemon ./
strip -s -x transmission-daemon
xz -zf9e transmission-daemon
tar -cJC $TR_INSDIR/share/transmission -f public_html.txz --numeric-owner --owner=0 --group=0 public_html
