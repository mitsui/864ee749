#!/bin/bash
set -e
export HOME=$(pwd)
WORK_DIR=$HOME
DEPS_DIR=${DEPS_DIR:-"$WORK_DIR/_builds"}
export PKG_CONFIG='pkg-config --static'
export PKG_CONFIG_PATH=$DEPS_DIR/lib/pkgconfig
export PKG_CONFIG_LIBDIR=$DEPS_DIR/lib
export LD_LIBRARY_PATH=$DEPS_DIR/lib

mkdir -p $WORK_DIR
mkdir -p $DEPS_DIR/lib
ln -sf lib $DEPS_DIR/lib64 || true

b_zlib() {
  cd $WORK_DIR
  curl -L http://zlib.net/current/zlib.tar.gz | tar xz
  cd zlib-*
  ./configure --prefix=$DEPS_DIR --static
  make -j`nproc`
  make install
}

b_zstd() {
  cd $WORK_DIR
  curl -L https://github.com/facebook/zstd/releases/download/v1.5.6/zstd-1.5.6.tar.gz | tar xz
  cd zstd-*
  cmake build/cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DZSTD_BUILD_CONTRIB=0 -DZSTD_BUILD_TESTS=0 -DZSTD_LEGACY_SUPPORT=0 -DZSTD_BUILD_PROGRAMS=0 -DZSTD_PROGRAMS_LINK_SHARED=0 -DZSTD_BUILD_SHARED=0 -DZSTD_BUILD_STATIC=1 -DZSTD_MULTITHREAD_SUPPORT=1
  make -j`nproc` -C_b install
}

b_brotli() {
  cd $WORK_DIR
  curl -L https://github.com/google/brotli/archive/refs/tags/v1.1.0.tar.gz | tar xz
  cd brotli-*
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DBUILD_SHARED_LIBS=0 -DBROTLI_DISABLE_TESTS=1
  make -j`nproc` -C_b install
}

b_cares() {
  cd $WORK_DIR
  curl -L https://github.com/c-ares/c-ares/releases/download/v1.34.4/c-ares-1.34.4.tar.gz | tar xz
  cd c-ares-*
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DBUILD_SHARED_LIBS=OFF -DCARES_STATIC=ON -DCARES_STATIC_PIC=1 -DCARES_SHARED=0 -DCARES_BUILD_TESTS=0 -DCARES_BUILD_CONTAINER_TESTS=0 -DCARES_BUILD_TOOLS=0
  make -j`nproc` -C_b install
  #./configure --prefix=$DEPS_DIR --disable-tests --enable-static --disable-shared
  #make -j`nproc`
  #make install
}

b_libressl() {
  cd $WORK_DIR
  curl -L https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/libressl-4.0.0.tar.gz | tar xz
  cd libressl-*
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DCMAKE_PREFIX_PATH=$DEPS_DIR -DBUILD_SHARED_LIBS=0 -DLIBRESSL_APPS=0 -DLIBRESSL_TESTS=0 -DENABLE_NC=0
  make -j`nproc` -C_b install
  #./configure --prefix=$DEPS_DIR --disable-tests --enable-static --disable-shared
  #make -j`nproc`
  #make install && rm -rf $DEPS_DIR/share
}

b_ssl() {
  cd $WORK_DIR
  git clone --single-branch --depth 1 -b openssl-3.0.13+quic https://github.com/quictls/openssl
  cd openssl
  ./config no-zlib enable-tls1_3 no-shared --prefix=$DEPS_DIR
  make -j`nproc`
  make install_sw
}

b_event() {
  cd $WORK_DIR
  curl -L https://github.com/libevent/libevent/releases/download/release-2.1.12-stable/libevent-2.1.12-stable.tar.gz | tar xz
  cd libevent-*
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DCMAKE_PREFIX_PATH=$DEPS_DIR -DEVENT__DISABLE_OPENSSL=1 -DEVENT__DISABLE_BENCHMARK=1 -DEVENT__LIBRARY_TYPE=STATIC -DEVENT__DISABLE_TESTS=1 -DEVENT__DISABLE_REGRESS=1 -DEVENT__DISABLE_SAMPLES=1
  make -j`nproc` -C_b install
  #./configure --prefix=$DEPS_DIR --enable-static --disable-shared --disable-samples --disable-openssl
  #make -j`nproc`
  #make install
}

b_iconv() {
  cd $WORK_DIR
  curl -L https://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.18.tar.gz | tar xz
  cd libiconv-*
  ./configure --prefix=$DEPS_DIR --enable-static --disable-shared
  make -j`nproc`
  make install
}

b_unistring() {
  cd $WORK_DIR
  curl -L https://ftp.gnu.org/gnu/libunistring/libunistring-1.3.tar.xz | tar xJ
  cd libunistring-*
  ./configure --prefix=$DEPS_DIR --enable-static --disable-shared
  make -j`nproc`
  make install
}

b_idn2() {
  cd $WORK_DIR
  curl -L https://ftp.gnu.org/gnu/libidn/libidn2-latest.tar.gz | tar xz
  cd libidn2-*
  ./configure --prefix=$DEPS_DIR --enable-static --disable-shared --disable-doc
  make -j`nproc`
  make install
}

b_psl() {
  cd $WORK_DIR
  curl -L https://github.com/rockdaboot/libpsl/releases/download/0.21.5/libpsl-0.21.5.tar.gz | tar xz
  cd libpsl-*
  ./configure --prefix=$DEPS_DIR --disable-gtk-doc-html --disable-man --disable-builtin --disable-shared --enable-static --enable-runtime=libidn2
  make -j`nproc`
  make install
}

b_nghttp2() {
  cd $WORK_DIR
  curl -L https://github.com/nghttp2/nghttp2/releases/download/v1.64.0/nghttp2-1.64.0.tar.gz | tar xz
  cd nghttp2-*
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DCMAKE_PREFIX_PATH=$DEPS_DIR -DBUILD_SHARED_LIBS=0 -DBUILD_STATIC_LIBS=1 -DENABLE_LIB_ONLY=1 -DENABLE_DOC=0 -DBUILD_TESTING=0 -DOPENSSL_INCLUDE_DIR= -DLIBXML2_INCLUDE_DIR=
  make -j`nproc` -C_b install
  #./configure --prefix=$DEPS_DIR --disable-app --enable-lib-only --enable-static --disable-shared
  #make -j`nproc`
  #make install
}

b_curl() {
  cd $WORK_DIR
  curl -L https://curl.se/download/curl-8.12.1.tar.gz | tar xz
  cd curl-*
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$DEPS_DIR -DCMAKE_PREFIX_PATH=$DEPS_DIR -DBUILD_SHARED_LIBS=0 -DBUILD_STATIC_LIBS=1 -DBUILD_STATIC_CURL=1 -DBUILD_LIBCURL_DOCS=0 -DENABLE_CURL_MANUAL=0 -DBUILD_TESTING=0 -DCURL_BROTLI=ON -DCURL_ZSTD=1 -DCURL_USE_OPENSSL=1 -DCURL_DISABLE_OPENSSL_AUTO_LOAD_CONFIG=1 -DCURL_USE_LIBSSH=0 -DCURL_USE_LIBSSH2=0 -DUSE_NGHTTP2=1 -DUSE_NGHTTP3=0 -DUSE_NGTCP2=0 -DENABLE_ARES=1 -DCMAKE_C_FLAGS="-DCARES_STATICLIB -DNGHTTP2_STATICLIB -DNGHTTP3_STATICLIB -DNGTCP2_STATICLIB"
  sed -i 's|$| -L'$DEPS_DIR'/lib -lidn2 -lunistring -liconv|' _b/src/CMakeFiles/curl.dir/link.txt
  make -C_b -j`nproc` install
  #LDFLAGS="-L$DEPS_DIR/lib" LIBS="-lidn2 -lunistring -liconv -lssl -lcrypto -lbrotlicommon -lpthread -ldl" ./configure --prefix=$DEPS_DIR --enable-optimize --enable-ares --enable-proxy --enable-gopher --enable-libcurl-option --enable-ipv6 --enable-static --disable-debug --disable-curldebug --disable-manual --disable-shared --with-ssl --without-gnutls
  #make -j`nproc`
  #make install
}

[ -f $DEPS_DIR/lib/libz.a ] || b_zlib
[ -f $DEPS_DIR/lib/libzstd.a ] || b_zstd
[ -f $DEPS_DIR/lib/libbrotlidec.a -a -f $DEPS_DIR/lib/libbrotlicommon.a ] || b_brotli
[ -f $DEPS_DIR/lib/libcares.a ] || b_cares
[ -f $DEPS_DIR/lib/libcrypto.a -a -f $DEPS_DIR/lib/libssl.a ] || b_libressl
[ -f $DEPS_DIR/lib/libevent.a ] || b_event
[ -f $DEPS_DIR/lib/libiconv.a ] || b_iconv
[ -f $DEPS_DIR/lib/libunistring.a ] || b_unistring
[ -f $DEPS_DIR/lib/libidn2.a ] || b_idn2
[ -f $DEPS_DIR/lib/libpsl.a ] || b_psl
[ -f $DEPS_DIR/lib/libnghttp2.a ] || b_nghttp2
[ -f $DEPS_DIR/lib/libcurl.a ] || b_curl
rm -rf $DEPS_DIR/share
