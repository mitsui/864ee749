#!/bin/sh
cp handshake.cc handshake.cc.bak
cp handshake.h handshake.h.bak
sed -i '/tr_handshake::ParseResult tr_handshake::parse_handshake(/,/return ParseResult::Ok;/s/set_peer_id(peer_id);/set_peer_id(peer_id);if (is_bad_peer(peer_id)) return ParseResult::EncryptionWrong;/' handshake.cc
sed -i '/#include <string_view>/a\#include <cstring>' handshake.h
sed -i '/static void on_error(/a\static bool is_bad_peer(tr_peer_id_t peer_id) {\
const char cid[] = {peer_id[1], peer_id[2]};\
if (!memcmp(cid, "XL", 2)) return true;\
if (!memcmp(cid, "SD", 2)) return true;\
if (!memcmp(cid, "QD", 2)) return true;\
if (!memcmp(cid, "XF", 2)) return true;\
if (!memcmp(cid, "BN", 2)) return true;\
if (!memcmp(cid, "DL", 2)) return true;\
if (!memcmp(cid, "GT", 2)) return true;\
return false;}' handshake.h